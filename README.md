The API can be accessed at:

http://88.80.187.61:5000

POST requests (with the appropriate request body) to http://88.80.187.61:5000 will return a shortened URL of the
format: 

{
  "shortened_url": "http://88.80.187.61:5000/<\d+>"
}

GET requests to http://88.80.187.61:5000/<\d+> will redirect to the original
URL

If there is no response from the server, please tell me as the redis cache and/or
Flask server may need restarting

You can access the repo at:
https://bitbucket.org/drsjmiddleton/babylon_test/src/master/