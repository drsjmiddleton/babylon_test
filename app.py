import json
import redis
from validators.url import url as validate_url
from flask import Flask, request, redirect, abort, jsonify


app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)
key_counter = "@@Key_counter&&"


@app.route('/', methods=['POST'])
def get_shortened_url():
    try:
        url = json.loads(request.data.decode('utf-8')).get('url')
    except json.JSONDecodeError:
        abort(400, "Please provide a url to shorten")

    if not url:
        abort(400, "Please provide a url to shorten")

    url = url if '://' in url else 'http://{}'.format(url)

    if not validate_url(url):
        abort(400, "Please provide a valid url")

    key = cache.get(url)
    if key:
        return jsonify({'shortened_url': '{}{}'.format(request.base_url, key.decode('utf-8'))})

    # In production I would almost certainly hash the url to produce the key
    new_key = cache.incr(key_counter)

    with cache.pipeline() as pipe:
        try:
            pipe.watch(url)
            pipe.multi()
            pipe.set(new_key, url)
            pipe.set(url, new_key)
            pipe.execute()
        except redis.WatchError:
            pass
    key = cache.get(url).decode('utf-8')
    return jsonify({'shortened_url': '{}{}'.format(request.base_url, key)})


@app.route('/<int:url_key>', methods=['GET'])
def url_redirect(url_key):
    url = cache.get(url_key)
    if not url:
        abort(400, "This does not match a url stored here.")
    return redirect(url.decode('utf-8'))


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
