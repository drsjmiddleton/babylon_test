import unittest
import json
import requests


class TestApi(unittest.TestCase):

    # Utilises https://jsonplaceholder.typicode.com/
    def test_api(self):
        shortened_url = json.loads(
            requests.post(
                'http://88.80.187.61:5000',
                data=json.dumps({"url": "https://jsonplaceholder.typicode.com/todos/1"})
            ).text
        ).get('shortened_url')
        dict_from_redirect = json.loads(requests.get(shortened_url).text)
        dict_from_api = {
            "userId": 1,
            "id": 1,
            "title": "delectus aut autem",
            "completed": False
        }
        self.assertEqual(dict_from_redirect, dict_from_api)

        self.assertEqual(
            400,
            requests.post(
                'http://88.80.187.61:5000',
                data=json.dumps({"foo": "json://placeholder.typicode.com/todos/1"})
            ).status_code
        )

        self.assertEqual(
            400,
            requests.post(
                'http://88.80.187.61:5000'
            ).status_code
        )

        self.assertEqual(
            400,
            requests.post(
                'http://88.80.187.61:5000',
                data=json.dumps({"url": "json://placeholder.typicode.com/todos/1"})
            ).status_code
        )

        self.assertEqual(
            400,
            requests.get('http://88.80.187.61:5000/0').status_code
        )
